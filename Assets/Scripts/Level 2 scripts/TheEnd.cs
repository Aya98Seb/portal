using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheEnd : MonoBehaviour
{

    [SerializeField] private GameObject TheEndObj;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {

            TheEndObj.SetActive(true);
        }
    }
}
