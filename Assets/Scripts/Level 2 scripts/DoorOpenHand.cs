using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenHand : MonoBehaviour
{
    private GameObject player;
    private Vector3 playerHandVector;
    [SerializeField] private GameObject moveHandButton;

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerController.Instance.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        playerHandVector = player.transform.position - transform.position;
        if (playerHandVector.sqrMagnitude < 4 && Vector3.Dot(player.transform.forward, transform.forward) > 0.3)
        {
            moveHandButton.SetActive(true);
        }
        else
        {
            if (moveHandButton.activeSelf == true)
            {
                moveHandButton.SetActive(false);

            }
        }
    }
}
