using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorEnd : MonoBehaviour
{
    [SerializeField] private Animation DoorFinalAnim;
    [SerializeField] private GameObject Explosion;
    [SerializeField] private GameObject StonDoor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            DoorFinalAnim.Play("OpenDoorEnd");
            StartCoroutine(ExplosiononDoor());
        }
    }

    IEnumerator ExplosiononDoor()
    {
        yield return new WaitForSeconds(1);
        Instantiate(Explosion, StonDoor.transform.position, Quaternion.identity);
        Destroy(StonDoor);
        Destroy(gameObject);

    }
}
