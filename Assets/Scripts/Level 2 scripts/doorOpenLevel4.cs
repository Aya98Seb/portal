using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class doorOpenLevel4 : MonoBehaviour
{

    [SerializeField] private Button YellowButtton;
    [SerializeField] private Button RedButtton;
    [SerializeField] private Button BlueButtton;
    private bool YellowDown;
    private bool RedDown;
    private bool BlueDown;

    [SerializeField] private Animation YellowAnim;
    [SerializeField] private Animation RedAnim;
    [SerializeField] private Animation BlueAnim;

    [SerializeField] private Animation DoorLeft;
    [SerializeField] private Animation DoorRight;

    [SerializeField] private GameObject RedMove;
    [SerializeField] private GameObject YellowMove;
    [SerializeField] private GameObject BlueMove;










    // Start is called before the first frame update
    void Start()
    {
        YellowButtton.onClick.AddListener(YellowMoveHand);
        RedButtton.onClick.AddListener(RedMoveHand);
        BlueButtton.onClick.AddListener(BlueMoveHand);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void YellowMoveHand()
    {
        YellowDown = !YellowDown;
        if(YellowDown && BlueDown && !RedDown)
        {
            DoorLeft.Play("OpenLeft");
            DoorRight.Play("OpenRight");
            destoyUnnecessaryObjects();

        }

        if (YellowDown)
        {
            YellowAnim.Play("MoveHandDownYellow");
        }
        else
        {
            YellowAnim.Play("MoveHandUpYellow");

        }
    }

    private void RedMoveHand()
    {

        
        RedDown = !RedDown;
        if (YellowDown && BlueDown && !RedDown)
        {
            DoorLeft.Play("OpenLeft");
            DoorRight.Play("OpenRight");
            destoyUnnecessaryObjects();

        }

        if (RedDown)
        {
            RedAnim.Play("MoveHandDownRed");
        }
        else
        {
            RedAnim.Play("MoveHandUpRed");

        }
    }

    private void BlueMoveHand()
    {
        BlueDown = !BlueDown;
        if (YellowDown && BlueDown && !RedDown)
        {
            DoorLeft.Play("OpenLeft");
            DoorRight.Play("OpenRight");
            destoyUnnecessaryObjects();
        }

        if (BlueDown)
        {
            BlueAnim.Play("MoveHandDownBlue");
        }
        else
        {
            BlueAnim.Play("MoveHandUpBlue");

        }
    }

    private void destoyUnnecessaryObjects()
    {
        Destroy(RedMove);
        Destroy(YellowMove);
        Destroy(BlueMove);
        Destroy(RedButtton.gameObject);
        Destroy(YellowButtton.gameObject);
        Destroy(BlueButtton.gameObject);

    }
}
