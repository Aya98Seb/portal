using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTransform : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayerController.Instance.transform.position = transform.position;
        PlayerController.Instance.transform.rotation = transform.rotation;
        StartCoroutine(ResetHealth());
    }

    IEnumerator ResetHealth()
    {
        yield return new WaitForSeconds(0.1f);
        PlayerController.Instance.gameObject.GetComponent<PlayerHealth>().Heal();

    }
}
