using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private FixedJoystick Joystick;
    private float xMovement;
    private float zMovement;
    private Vector3 moveVector;
    private CharacterController controller;
    [SerializeField] private float movingSpeed;
    [SerializeField] private float gravity;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform usedcamera;
    [SerializeField] private GameObject canvasToNotDestroy;

    public static PlayerController Instance { get; private set; }


    private void Awake()
    {

        

        if (Instance != null && Instance != this)
        {
            Destroy(this);
            Destroy(this.gameObject);

        }
        else
        {
            Instance = this;
        }

        DontDestroyOnLoad(Instance.gameObject);
        DontDestroyOnLoad(usedcamera);
        DontDestroyOnLoad(canvasToNotDestroy);

    }
    void Start()
    {
        
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {


        animator.SetFloat("movingSpeed", moveVector.sqrMagnitude);
        //moveVector = transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical");

         moveVector = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
         moveVector = Quaternion.Euler(0, usedcamera.rotation.eulerAngles.y, 0) * moveVector;
        //Debug.Log(usedcamera.rotation.y);
        xMovement = Joystick.Horizontal;
        zMovement = Joystick.Vertical;
        //moveVector = new Vector3(xMovement, 0, zMovement);
        if (moveVector != Vector3.zero)
        {
            gameObject.transform.forward = moveVector.normalized;
        }


        controller.Move(moveVector * movingSpeed * Time.unscaledDeltaTime);
        controller.Move(new Vector3(0,gravity,0) * Time.unscaledDeltaTime);

    }

   
}
