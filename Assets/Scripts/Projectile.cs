using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float movingSpeed  = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf)
        {
            transform.position += transform.forward * movingSpeed * Time.unscaledDeltaTime;
        }

        //Alternative to OnTriggerEnter:
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, transform.localScale.x);
        foreach (var hitCollider in hitColliders)
        {
            if (!hitCollider.CompareTag("Player") && !hitCollider.CompareTag("Projectile") && !hitCollider.CompareTag("Feild")) 
            {

                gameObject.SetActive(false);
            }

            if (hitCollider.CompareTag("Enemy"))
            {
                  Debug.Log("Attack");
                hitCollider.GetComponent<Enemy>().playerAttackEnemy();

            }
        }

    }
    //OverlapSphereNonAlloc

    // private void OnTriggerEnter(Collider other)
    // {
    //this is not working with freeze

    // if (!other.CompareTag("Player")) 
    // {

    // gameObject.SetActive(false);
    // }

    // if (other.CompareTag("Enemy"))
    // {
    //  Debug.Log("Attack");
    //}
    //}


}
