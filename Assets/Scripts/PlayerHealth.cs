using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    private int playerHealth = 100;
    private bool gameOver = false;
    [SerializeField] private GameObject EmissiveObject;
    private Material playerMat;
    private Color emmisiveColor;
    private float emissionIntensity = 1;
    [SerializeField] private GameObject MenuPanel;
    // Start is called before the first frame update
    void Start()
    {

        playerMat = EmissiveObject.GetComponent<Renderer>().material;
        emmisiveColor = playerMat.GetColor("_EmissionColor");
        
    }

    // Update is called once per frame
    void Update()
    {
        if(playerHealth == 0)
        {
            gameOver = true;
            MenuPanel.SetActive(true);
            PlayerController.Instance.gameObject.GetComponentInChildren<Animator>().SetBool("enemyAttackPlayer", false);

            Time.timeScale = 0;
            PlayerController.Instance.enabled = false;
        }

    }

    public void Attacked()
    {
        playerHealth--;
        emissionIntensity -= 0.01f;

        playerMat.SetColor("_EmissionColor" , emmisiveColor * emissionIntensity);
        Debug.Log(playerHealth);
    }

    public void Heal()
    {
        playerHealth = 100;
        emissionIntensity = 1f;

        playerMat.SetColor("_EmissionColor", emmisiveColor * emissionIntensity);
        Debug.Log(playerHealth);
    }

    
}
