using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlabColor : MonoBehaviour
{
    [SerializeField] private GameObject enemy1;
    [SerializeField] private GameObject enemy2;
    [SerializeField] private GameObject Slab1;
    [SerializeField] private GameObject Slab2;
    private bool animation1Played;
    private bool animation2Played;






    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enemy1 == null && !animation1Played)
        {
            Slab1.GetComponent<Animation>().Play("Slab1Color");
            animation1Played = true;
            if (enemy1 == null && enemy2 == null)
            {
                Destroy(gameObject);
            }
        }
        if (enemy2 == null && !animation2Played)
        {
            Slab2.GetComponent<Animation>().Play("Slab2Color");
            animation2Played = true;
            if (enemy1 == null && enemy2 == null)
            {
                Destroy(gameObject);
            }

        }
        
    }
}
