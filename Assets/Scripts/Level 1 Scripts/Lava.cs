using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    private int damage = 20;
    [SerializeField] private Transform respawnplace;
    [SerializeField] Animator playerAnimator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            for (int i = 0; i < damage; i++)
            {
                other.GetComponent<PlayerHealth>().Attacked();
            }
            StartCoroutine(respawnPlayer(other.gameObject));
        }
    }

    IEnumerator respawnPlayer(GameObject player)
    {
        playerAnimator.SetBool("enemyAttackPlayer", true);

        yield return new WaitForSeconds(2);
        player.transform.position = respawnplace.position;
        playerAnimator.SetBool("enemyAttackPlayer", false);

    }
}
