using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorOpen : MonoBehaviour
{

     private GameObject player;
    private Vector3 playerPalletVector;
    [SerializeField] private GameObject openDoorButton;
    private bool onPalletShowing;
    [SerializeField] private GameObject Pallet;
    [SerializeField] private Button[] colorButtons;
    private int numberOfColorSelected;
    private int numberOfTrueColorClicked;

    [SerializeField] private GameObject doorJointLeft;
    [SerializeField] private GameObject doorJointRight;


    // Start is called before the first frame update
    void Start()
    {
        player = PlayerController.Instance.gameObject;
        openDoorButton.GetComponent<Button>().onClick.AddListener(ShowPallet);
        SetColorButtonsFunctions();
    }

    // Update is called once per frame
    void Update()
    {
        playerPalletVector = player.transform.position - transform.position;
        if(playerPalletVector.sqrMagnitude<9 && Vector3.Dot(player.transform.forward, transform.forward) > 0.3 && !onPalletShowing)
        {
            openDoorButton.SetActive(true);
        }
        else
        {
            if(openDoorButton.activeSelf == true)
            {
                openDoorButton.SetActive(false);

            }
        }
        
    }

    private void ShowPallet()
    {
        onPalletShowing = true;
        openDoorButton.SetActive(false);
        Pallet.SetActive(true);
    }

    private void SetColorButtonsFunctions()
    {
        for (int i = 0; i< colorButtons.Length; i++)
        {
            colorButtons[i].onClick.AddListener(AllColorButtonFunction);
        }
        colorButtons[1].onClick.AddListener(TrueColorClicked);
        colorButtons[5].onClick.AddListener(TrueColorClicked);

    }

    private void AllColorButtonFunction()
    {
        numberOfColorSelected++;
        if(numberOfColorSelected >= 2)
        {
            Pallet.SetActive(false);
            numberOfColorSelected = 0;
            onPalletShowing = false;
            StartCoroutine(ensureTrueOrder());
        }
    }

    private void TrueColorClicked()
    {
        numberOfTrueColorClicked++;
        if(numberOfTrueColorClicked >= 2)
        {
            doorJointLeft.GetComponent<Animation>().Play("DoorOpenedLeft");
            doorJointRight.GetComponent<Animation>().Play("DoorOpenedRight");
            Destroy(gameObject);
        }
    }

    IEnumerator ensureTrueOrder()
    {
        yield return new WaitForEndOfFrame();
        numberOfTrueColorClicked = 0;

    }
}
