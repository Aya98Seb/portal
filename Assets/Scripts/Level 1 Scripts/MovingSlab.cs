using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingSlab : MonoBehaviour
{
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent = transform;
            Debug.Log("Player on Slab");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent = null;
            DontDestroyOnLoad(other.gameObject);
        }
    }
}
