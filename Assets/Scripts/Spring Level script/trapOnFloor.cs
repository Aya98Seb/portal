using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trapOnFloor : MonoBehaviour
{
    private Animation trapFloorAnim;
    // Start is called before the first frame update
    void Start()
    {
        trapFloorAnim = transform.parent.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            trapFloorAnim.Play("FloorTrap");
            Destroy(gameObject);
        }
    }
}
