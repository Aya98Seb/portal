using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorOpeneSpring : MonoBehaviour
{
     [SerializeField] private Button MoveButtton;
    private Animation handAnimation;
    [SerializeField] private Animation DoorLeft;
    [SerializeField] private Animation DoorRight;
    [SerializeField] private enemyManualAI[] enemiesInsideDoor; 

    // Start is called before the first frame update
    void Start()
    {
        MoveButtton.onClick.AddListener(moveHand);
        handAnimation = transform.parent.GetComponentInChildren<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void moveHand()
    {
        handAnimation.Play("MoveHandDownRed");
        DoorLeft.Play("DoorOpenLeft");
        DoorRight.Play("DoorOpenRight");

        if (enemiesInsideDoor.Length != 0)
        {
            for (int i = 0; i < enemiesInsideDoor.Length; i++)
            {
                enemiesInsideDoor[i].enabled = true;
            }
        }

        Destroy(MoveButtton.gameObject);
        Destroy(gameObject);

    }

}
