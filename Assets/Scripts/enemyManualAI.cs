using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyManualAI : MonoBehaviour
{
    private float speed = 3;
    RaycastHit hit;
    private GameObject player;
    private Animator enemyAnimator;
    private float sqrtDistanceFromPlayer;
    private bool obstacleInFront = false;
    private Vector3 tangentVector;
    private Vector3 vectorToPlayer;
    private Vector3 vectorPlayerHitPoint;
    private Vector3 vectortoObs;



    // Start is called before the first frame update
    private void Start()
    {
        player = PlayerController.Instance.gameObject;
        enemyAnimator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z), transform.up);

        sqrtDistanceFromPlayer = (player.transform.position - transform.position).sqrMagnitude;
        if (sqrtDistanceFromPlayer >= 1 && sqrtDistanceFromPlayer <= 64)
        {
            enemyAnimator.SetBool("isMoving", true);
            if (!obstacleInFront)
            {

                transform.position += transform.forward * speed * Time.deltaTime;
            }


            if (Physics.Raycast(transform.position, transform.forward, out hit, 1.5f, 1 << 9))
            {
                obstacleInFront = true;
                Debug.Log("Obstacle in front");

                tangentVector = Quaternion.Euler(0, 90, 0) * new Vector3(hit.normal.x, 0, hit.normal.z);
                vectorToPlayer = (player.transform.position - transform.position).normalized;
                vectorPlayerHitPoint = (player.transform.position - hit.point).normalized;
                vectortoObs = (hit.transform.position - transform.position).normalized;


                if (Mathf.Abs(Vector3.Dot(vectorPlayerHitPoint, tangentVector)) > 0.7071)
                {

                    if (Vector3.Dot(tangentVector, vectorToPlayer) > 0)
                    {
                        transform.position += tangentVector * speed * Time.deltaTime;

                    }
                    else
                    {
                        transform.position -= tangentVector * speed * Time.deltaTime;

                    }
                }
                else
                {
                    if (Vector3.Dot(tangentVector, vectortoObs) > 0)
                    {
                        transform.position -= tangentVector * speed * Time.deltaTime;

                    }
                    else
                    {
                        transform.position += tangentVector * speed * Time.deltaTime;

                    }
                }


            }
            else
            {
                obstacleInFront = false;
            }
        }
        else
        {
            enemyAnimator.SetBool("isMoving", false);

        }




    }
}
