using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [SerializeField] private int enemyHealth = 3;
     private Animator playerAnimator;
     private Animator enemyAnimator;

     private GameObject feild;
    [SerializeField] private GameObject DieEffect;


    // Start is called before the first frame update
    void Start()
    {
        playerAnimator = PlayerController.Instance.GetComponentInChildren<Animator>();
        feild = transform.GetChild(1).gameObject;
        enemyAnimator = GetComponentInChildren<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
     
     if(enemyHealth == 0)
        {
            StartCoroutine(EnemyDie());
        }
    }

    public void playerAttackEnemy()
    {

        enemyHealth--;
        Debug.Log(enemyHealth);

    }

    IEnumerator EnemyDie()
    {
        feild.SetActive(false);
        GetComponent<enemyManualAI>().enabled = false;
        playerAnimator.SetBool("enemyAttackPlayer", false);
        enemyAnimator.SetBool("die", true);

        yield return new WaitForSeconds(1);
        GameObject effect = Instantiate(DieEffect, transform.position, Quaternion.identity);

        Destroy(gameObject);
        


    }
}
