using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerFire : MonoBehaviour
{
    [SerializeField] private GameObject projectile1;
    private List<GameObject> projectilesList = new List<GameObject>();
    [SerializeField] private int numberOfProjectile;
    [SerializeField] private float secondsForActiveProjectile;
    [SerializeField] private float offsetOfPojectileFromPlayery;
    [SerializeField] private Animator animator;
    [SerializeField] private Button shootButton;
    [SerializeField] private Button freezButton;
    private bool canFreeze = true;
    private bool canFire = true;
    [SerializeField] private GameObject projectileEffect;
    private List<GameObject> projectileEffectList = new List<GameObject>();



    

    // Start is called before the first frame update
    void Start()
    {

        shootButton.onClick.AddListener(FireWithAnimation);
        freezButton.onClick.AddListener(Freeze);

        createStarterProjectiles();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire2"))
        {
            FireWithAnimation();
        }
    }

    private void createStarterProjectiles()
    {
        for (int i = 0; i<numberOfProjectile; i++)
        {
            GameObject startProjectile = Instantiate(projectile1 );
            DontDestroyOnLoad(startProjectile);

            projectilesList.Add(startProjectile);
            startProjectile.SetActive(false);

        }
        for (int i = 0; i < numberOfProjectile; i++)
        {
            GameObject startProjectileeffect = Instantiate(projectileEffect );
            DontDestroyOnLoad(startProjectileeffect);
            projectileEffectList.Add(startProjectileeffect);
            startProjectileeffect.SetActive(false);

        }
    }

    private void Fire()
    {

        GameObject projectileOnFire = projectilesList[0];
        projectileOnFire.transform.position = transform.position + new Vector3(0,offsetOfPojectileFromPlayery,0) ;
        projectileOnFire.transform.forward = transform.forward;
        projectileOnFire.SetActive(true);
        projectilesList.RemoveAt(0);
        projectilesList.Add(projectileOnFire);

        StartCoroutine(addeffect(projectileOnFire));

        StartCoroutine( deactivateProjectile(projectileOnFire));


    }

    IEnumerator deactivateProjectile(GameObject theProjectile)
    {
        yield return new WaitForSecondsRealtime(secondsForActiveProjectile);
        theProjectile.SetActive(false);

        

    }

    private void FireWithAnimation()
    {
        StartCoroutine(shootingWithDelay());
    }

    IEnumerator shootingWithDelay()
    {
        if (canFire)
        {
            animator.SetBool("isAttacking", true);
            canFire = false;
            yield return new WaitForSecondsRealtime(0.7f);

            Fire();
            yield return new WaitForSecondsRealtime(0.3f);
            animator.SetBool("isAttacking", false);
            canFire = true;
        }

    }

    private void Freeze()
    {
        if (canFreeze)
        {
            Time.timeScale = 0.1f;
            canFreeze = false;
            StartCoroutine(stopFreez());
        }
    }

    IEnumerator stopFreez()
    {
        yield return new WaitForSecondsRealtime(5);
        Time.timeScale = 1;
        yield return new WaitForSecondsRealtime(5);
        canFreeze = true;
    }

    IEnumerator addeffect(GameObject theProjectile)
    {

        GameObject hitEffect = projectileEffectList[0];
        yield return new WaitWhile(() => theProjectile.activeSelf == true);
        hitEffect.transform.position = theProjectile.transform.position;
        hitEffect.SetActive(true);
        projectileEffectList.RemoveAt(0);
        projectileEffectList.Add(hitEffect);
        yield return new WaitForSecondsRealtime(1);
        hitEffect.SetActive(false);
    }

    public void DestroyProjectile()
    {
        foreach(GameObject Projectile in projectilesList)
        {
            Destroy(Projectile);
        }

        foreach (GameObject Projectileeffect in projectileEffectList)
        {
            Destroy(Projectileeffect);
        }
    }

}
