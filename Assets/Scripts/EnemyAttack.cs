using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private GameObject player;
    private bool canAttack = true;
    [SerializeField] private GameObject lightAbsorb;
    private List<GameObject> LightAbsorbList = new List<GameObject>();
    private int numberOfLightObsorbObjects = 1;
    private GameObject lighAbsorbOnUse;
    private Animator enemyAnimator;
     private Animator PlayerAnimator;

    

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerController.Instance.gameObject;
        PlayerAnimator = player.GetComponentInChildren<Animator>();
        enemyAnimator = transform.parent.GetComponentInChildren<Animator>(); 
        creatStarterObjects();
    }

    // Update is called once per frame
    void Update()
    {
        //to fix small issue if enemy is moving
        if (lighAbsorbOnUse!= null)
        {
            if (lighAbsorbOnUse.activeSelf)
            {
                lighAbsorbOnUse.transform.LookAt(transform);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AttackPlayer();
            enemyAnimator.SetBool("isAttacking", true);
            PlayerAnimator.SetBool("enemyAttackPlayer", true);

        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            enemyAnimator.SetBool("isAttacking", false);
            PlayerAnimator.SetBool("enemyAttackPlayer", false);

        }
    }


    private void AttackPlayer()
    {
        if (canAttack)
        {
            lighAbsorbOnUse = LightAbsorbList[0];
            lighAbsorbOnUse.transform.forward = -transform.forward;
            lighAbsorbOnUse.transform.position = player.transform.position;
            lighAbsorbOnUse.SetActive(true);
            canAttack = false;
            player.GetComponent<PlayerHealth>().Attacked();
            StartCoroutine(ResetCanAttack());
            
        }
    }

    private void creatStarterObjects()
    {
        for(int  i = 0; i < numberOfLightObsorbObjects; i++)
        {
            GameObject startObj = Instantiate(lightAbsorb);
            LightAbsorbList.Add(startObj);
            startObj.SetActive(false);
        }
    }

    IEnumerator ResetCanAttack()
    {
        yield return new WaitWhile(() => LightAbsorbList[0].activeSelf == true);
        canAttack = true;

    }

    
}
