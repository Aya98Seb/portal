using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour
{

    [SerializeField] private GameObject MenuPanel;
    [SerializeField] private GameObject MenuExit;
    [SerializeField] private Button buttonOFMenu;
    [SerializeField] private Button MenuExitButton;
    [SerializeField] private Button ReplayButton;
    [SerializeField] private Button QuitButtton;

    [SerializeField] private GameObject[] dontDestroyOnLoadObject;


    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        buttonOFMenu.onClick.AddListener(Menu);
        MenuExitButton.onClick.AddListener(ExitTheMenu);
        ReplayButton.onClick.AddListener(Replay);
        QuitButtton.onClick.AddListener(Quit);

    }

    // Update is called once per frame
    

    private void Quit()
    {
        Application.Quit();
    }

    private void Replay()
    {
        Time.timeScale = 1;
        PlayerController.Instance.enabled = true;
        PlayerController.Instance.gameObject.GetComponent<PlayerHealth>().Heal();
        PlayerController.Instance.gameObject.GetComponentInChildren<Animator>().SetBool("enemyAttackPlayer", false);

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            PlayerController.Instance.gameObject.GetComponent<PlayerFire>().DestroyProjectile();
            for (int i = 0; i<dontDestroyOnLoadObject.Length; i++)
            {
                Destroy(dontDestroyOnLoadObject[i]);
            }
        }
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            Destroy(gameObject);
        }
        ExitTheMenu();

    }

    private void Menu()
    {
        MenuPanel.SetActive(true);
        MenuExit.SetActive(true);
        Time.timeScale = 0;
        PlayerController.Instance.enabled = false;

    }

    private void ExitTheMenu()
    {
        MenuPanel.SetActive(false);
        MenuExit.SetActive(false);
        Time.timeScale = 1;
        PlayerController.Instance.enabled = true;
    }
}
